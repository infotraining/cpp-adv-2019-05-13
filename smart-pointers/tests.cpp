#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

struct Gadget
{
	string name;

	Gadget(string n) : name(std::move(n))
	{
		cout << "Gadget(" << name << ")\n";
	}

	Gadget(const Gadget&) = default;
	Gadget& operator=(const Gadget&) = default;
	Gadget(Gadget&&) noexcept = default;
	Gadget& operator=(Gadget&&) = default;

	~Gadget()
	{
		cout << "~Gadget(" << name << ")" << endl;
	}
};

TEST_CASE("unique_ptr")
{
	vector<unique_ptr<Gadget>> gadgets;

	gadgets.emplace_back(make_unique<Gadget>("ipad2"));
	gadgets.push_back(make_unique<Gadget>("mp3 player"));
	gadgets.push_back(make_unique<Gadget>("smartphone"));

	auto ptr = make_unique<Gadget>("ipad");
	gadgets.push_back(std::move(ptr));

	cout << gadgets[0]->name << endl;
}

namespace Legacy
{
	int* create_array(size_t size)
	{
		return new int[size];
	}
}

TEST_CASE("unique_ptr with arrays")
{
	unique_ptr<int[]> tab(Legacy::create_array(10));

	for (int i = 0; i < 10; ++i)
		tab[i] = i * 2;
}

TEST_CASE("unique_ptr with custom dealloc")
{
	unique_ptr<FILE, int(*)(FILE*)> f(fopen("text.abc", "w"), &fclose);

	string filename = "filename";
	auto closer = [=](FILE* f) { fclose(f); cout << filename << " is closed!\n"; };
	unique_ptr<FILE, decltype(closer)> f2(fopen("backup.abc", "w"), closer);
	cout << sizeof(f2) << endl;

	fprintf(f.get(), "text");
	fprintf(f2.get(), "text - backup");
}

TEST_CASE("shared_ptr with custom dealloc")
{	
	shared_ptr<FILE> f2(fopen("backup.abc", "w"), [](FILE* f) { 
		if (f) // ! important check
			fclose(f); 
		cout << "File is closed!\n"; }
	);
	
	fprintf(f2.get(), "text - backup");
}

TEST_CASE("lambda call efficiency")
{
	auto l1 = []() { cout << "lambda" << endl; };
	void(*l2)() = []() { cout << "lambda" << endl; };
	std::function<void()> l3 = []() { cout << "lambda" << endl; };

	l1();
	l2();
	l3();
}

//void call(std::function<void(int)> callable)
//{
//	callable(13);
//}

template <typename F>
void call(F&& callable)
{
	callable(13);
}
