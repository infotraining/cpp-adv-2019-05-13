#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

namespace Explain
{
	template <bool Condition, typename T = void>
	struct EnableIf
	{
		using type = T;
	};

	template <typename T>
	struct EnableIf<false, T>
	{};
}

struct Value
{
	operator double() const
	{
		return 3.14;
	}
};

template <>
struct is_floating_point<Value> : std::true_type
{
};

template <typename T>
typename Explain::EnableIf<is_floating_point<T>::value>::type process(T x)
{
	cout << "process(" << typeid(x).name() << ": " << x << ")\n";
}

template <typename T>
std::enable_if_t<is_integral_v<T>> process(T x)
{
	cout << "process(" << typeid(x).name()  ": " << x << ")\n";
}


TEST_CASE("test")
{
	process(42);

	short s = 12;
	process(s);

	process(3.14);
	process(3.14f);

	//process("string"s);

	process(Value{});

	int tab1[3] = { 1, 2, 3 };
	int tab2[3];

	std::copy(begin(tab1), end(tab1), begin(tab2));
}