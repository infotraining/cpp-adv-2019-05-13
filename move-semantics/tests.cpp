#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

string full_name(const string& first, const string& last)
{
	return first + " " + last;
}


TEST_CASE("reference binding")
{
	string name = "Jan";

	SECTION("C++98")
	{
		SECTION("l-value ref can be bound to l-value")
		{
			string& ref_name = name;
		}

		SECTION("r-value can be bound to l-value ref with const")
		{
			const string& ref_full_name = full_name(name, "Kowalski");	
		}
	}

	SECTION("C++11")
	{
		SECTION("r-value ref can be bound to r-value")
		{
			string&& ref_full_name = full_name(name, "Kowalski");

			ref_full_name[0] = 'P';

			cout << ref_full_name << endl;
		}

		SECTION("l-value can not be bound to r-value ref")
		{
			//string&& ref_name = name;
		}
	}
}

TEST_CASE("life after move")
{
	vector<int> vec = { 1, 2, 3 };

	vector<int> dest;

	dest = std::move(vec);

	//cout << vec.size() << endl; // UB

	vec = vector<int>{ 5, 6, 7, 8 }; // assignment a new state
}

class MyVector
{
	int* array_;
	size_t size_;

	void print_items(const string& msg)
	{
		cout << msg << " : { ";
		for (const auto& item : *this)
			cout << item << " ";
		cout << "}\n";
	}

public:
	using iterator = int*;
	using const_iterator = const int*;

	MyVector(size_t size, int value)
		: array_{new int[size]}, size_{size}
	{
		std::fill(begin(), end(), value);
		print_items("MyVector ctor");
	}

	MyVector(std::initializer_list<int> il)
		: array_{ new int[il.size()] }, size_{ il.size() }
	{
		std::copy(il.begin(), il.end(), array_);
		print_items("MyVector ctor");
	}

	MyVector(const MyVector& other)
		: array_{ new int[other.size()] }, size_{ other.size() }
	{
		std::copy(other.begin(), other.end(), this->begin());

		print_items("MyVector cc");
	}

	void swap(MyVector& other)
	{
		std::swap(array_, other.array_);
		std::swap(size_, other.size_);
	}

	MyVector& operator=(const MyVector& other)
	{
		MyVector temp(other);
		swap(temp);

		print_items("MyVector cc_op=");

		return *this;
	}

	// move contructor
	MyVector(MyVector&& other) noexcept
		: array_{other.array_}, size_{std::move(other.size_)}
	{
		other.array_ = nullptr;
		// extra work
		other.size_ = 0;

		print_items("MyVector mv");
	}

	// move assignment operator
	MyVector& operator=(MyVector&& other) noexcept
	{
		if (this != &other)
		{
			delete[] array_;

			array_ = other.array_;
			size_ = std::move(other.size_);
			
			other.array_ = nullptr;
			// extra work
			other.size_ = 0;
		}

		print_items("Move mv_op=");
		return *this;
	}

	~MyVector()
	{
		delete[] array_;
	}

	iterator begin()
	{
		return array_;
	}

	iterator end()
	{
		return array_ + size_;
	}

	const_iterator begin() const
	{
		return array_;
	}

	const_iterator end() const
	{
		return array_ + size_;
	}

	const_iterator cbegin() const
	{
		return array_;
	}

	const_iterator cend() const
	{
		return array_ + size_;
	}

	size_t size() const
	{
		return size_;
	}
};

MyVector create_vector()
{
	return MyVector{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; // RVO - mandatory since C++17	
}

MyVector create_vector_nrvo()
{
	MyVector vec{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	return vec; // Named RVO - nrvo
}

struct NoCopyableAndNoMoveable
{
	int x;

	NoCopyableAndNoMoveable(const NoCopyableAndNoMoveable&) = delete;
	NoCopyableAndNoMoveable& operator=(const NoCopyableAndNoMoveable&) = delete;
};

NoCopyableAndNoMoveable create_ncm()
{
	return { 13 };
}

TEST_CASE("No copyable & no moveable")
{
	NoCopyableAndNoMoveable ncm{ 10 };

	NoCopyableAndNoMoveable other = create_ncm();
}

TEST_CASE("Move semantics in class")
{
	MyVector vec = {1, 2, 3};

	for (const auto& item : vec)
	{
		cout << item << " ";
	}
	cout << endl;

	REQUIRE(vec.size() == 3);

	SECTION("Copy")
	{
		MyVector other = vec; // deep copy

		for (const auto& item : other)
			cout << item << " ";
		cout << endl;

		MyVector another = { 13, 14, 665 };
		other = another; // deep copy operator=
	}

	SECTION("Move")
	{
		cout << "\n-------- Move -------------\n";

		MyVector other = create_vector();

		for (const auto& item : other)
			cout << item << " ";
		cout << endl;

		vector<MyVector> container;

		container.push_back(MyVector{ 123, 124, 125 });
	}
}

class ImplicitMove
{
	int a;
	string b;
	std::unique_ptr<int> up;
public:
	ImplicitMove() : a(10), b("empty")
	{}	
};

TEST_CASE("Implicit move")
{
	ImplicitMove y;
	//Y other = y;
	ImplicitMove another = std::move(y);
}

class IData
{	
public:
	virtual void calculate() = 0;
	virtual ~IData() = default;
};

class Data : public IData
{
	int rank_;
	const string name_;
	MyVector data_;

public:
	Data(int rank, string name, MyVector data)
		: rank_(rank), name_(std::move(name)), data_(std::move(data))
	{}	

	void calculate() override {}
};

TEST_CASE("Data with move")
{
	cout << "\n---------Data----------\n";

	Data d1(1, "Dane", { 1, 2, 3 });

	Data d2 = d1;

	Data d3 = move(d1);
}

TEST_CASE("move doesn't move")
{
	cout << "\n----------const----------\n";
	const MyVector vec = { 1, 2, 3, 4 };
	MyVector other = move(vec);

	vector<Data> v;
	v.reserve(10);
}

/////////////////////////////////////
// universal reference
//

void foo(int&& value)
{
	cout << "foo(int&&" << value << ")\n";
}

template <typename T>
void foo_template(T&& value)
{
	puts(__FUNCSIG__);
	cout << "foo_template(T&&" << value << ")\n";
}

TEST_CASE("universal reference")
{
	foo(6);

	int x = 10;
	int&& ref_x = 10;
	//foo(x);

	foo_template(6);
	foo_template(ref_x);

	const auto cx = 1; // const int
	auto& aref_x = cx; // const int&
 	auto&& magic_ref1 = x; // const int&
	auto&& magic_ref2 = 2; // int&&
}


template <typename T>
class UniquePtr
{
	T* ptr_;
public:
	UniquePtr(T* ptr) noexcept: ptr_{ptr}
	{}

	UniquePtr(UniquePtr&& other) noexcept : ptr_{ other.ptr_ }
	{
		other.ptr_ = nullptr;
	}

	UniquePtr& operator=(UniquePtr&& other) noexcept
	{
		UniquePtr temp(std::move(other)); // noexcept
		swap(temp); // noexcept

		return *this;
	}

	~UniquePtr() noexcept
	{
		delete ptr_;
	}

	void swap(UniquePtr& other) noexcept
	{
		std::swap(ptr_, other.ptr_);
	}

	T& operator*() const noexcept
	{
		return *ptr_;
	}

	template <typename TResult = std::enable_if_t<is_class_v<T>, T*>>
	TResult operator->() const noexcept
	{
		return ptr_;
	}
};

template <typename T, typename Arg>
UniquePtr<T> my_make_unique(Arg&& arg)
{
	return UniquePtr<T>(new T(std::forward<Arg>(arg))); // perfect forwarding
}


TEST_CASE("Unique Ptr")
{
	cout << "\n----------forwading----------\n";

	MyVector vec{ 1, 2, 3 };
	UniquePtr<MyVector> uptr1 = my_make_unique<MyVector>(MyVector{ 6, 7, 8 });

	auto other = std::move(uptr1);
}

TEST_CASE("vector & move semantics")
{
	cout << "\n----------pushing to std::vector----------\n";
	std::vector<MyVector> vec;

	vec.push_back(MyVector{ 1, 2 });
	cout << "--\n";
	vec.push_back(MyVector{ 3, 4 });
	cout << "--\n";
	vec.emplace_back(5, 1);
	cout << "--\n";
	vec.emplace_back(5, 2);
	cout << "--\n";
	vec.push_back(MyVector{ 5, 6 });
	cout << "--\n";
	for (int i = 3; i < 10; ++i)
	{
		vec.emplace_back(5, i);
		cout << "--\n";
	}
}

string operator+(int a, string b)
{	
	return std::to_string(a) + b;
}

template <typename T>
auto safe(int x, T y) noexcept(noexcept(x + y))
{
	return x + y;
}

TEST_CASE("noexcept")
{
	REQUIRE(safe(1, "-one"s) == "1-one"s);
}