#include <utility>
#include <tuple>
#include <string>
#include <iostream>
#include <functional>
#include "catch.hpp"

namespace IndexSequenceImpl
{
    template <size_t... Is>
    struct IndexSequence
    {
        using type = IndexSequence<Is...>;
    };

    template <size_t N, size_t I, typename Seq>
    struct MakeSequence;

    template <size_t N, size_t I, size_t... Seq>
    struct MakeSequence<N, I, IndexSequence<Seq...>> : MakeSequence<N, I+1, IndexSequence<Seq..., I>>
    {};

    template <size_t N, size_t... Seq>
    struct MakeSequence<N, N, IndexSequence<Seq...>> : IndexSequence<Seq...>
    {};

    template <size_t N> 
    using MakeIndexSequence = typename MakeSequence<N, 0, IndexSequence<>>::type;

    static_assert(std::is_same_v<MakeIndexSequence<3>, IndexSequence<0, 1, 2>>);
}

template <typename F, typename T, size_t... I>
void apply_tuple_impl(F f, T const& t, std::index_sequence<I...>)
{
    (f(std::get<I>(t)), ...);
}

template <typename F, typename... Ts>
inline void tuple_apply(F&& f, std::tuple<Ts...> const& t)
{
    using Indexes = std::make_index_sequence<sizeof...(Ts)>;
    
    apply_tuple_impl(f, t, Indexes{});
}

TEST_CASE("apply for tuple")
{
    using namespace std::literals;

    auto printer = [](auto const& item) { 
        std::cout << "value: " << item << std::endl; 
    };

    auto tpl = std::make_tuple(1, 3.14, 'a', "text"s);

    tuple_apply(printer, tpl);
}

struct Data
{
    int a;
    double b;
    std::string c;

    auto tied() const
    {
        return std::tie(a, b, c);
    }    

	bool operator==(const Data& other) const
	{
		return tied() == other.tied();
	}
};

template <typename T>
void hash_combine(size_t& seed, const T& value)
{    
    seed ^= std::hash<T>{}(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct Hash
{
    size_t operator()(const Data& d) const
    {
        size_t seed = 0;

        tuple_apply([&seed](const auto& value) { hash_combine(seed, value); }, d.tied());

        return seed;        
    }
};

TEST_CASE("hash for data")
{   
    using namespace std::literals;

    Data d{1, 3.14, "string"s};

    REQUIRE(Hash{}(d) == 3511065252300346061);
}