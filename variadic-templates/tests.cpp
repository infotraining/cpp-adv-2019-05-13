#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

struct Data
{
	int a, b;
	double c;
	string s;
};


template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
	return unique_ptr<T>(new T{ std::forward<Args>(args)... });
}

TEST_CASE("my_make_unique")
{
	auto ptr1 = my_make_unique<Data>(1, 2, 4.5, "text"s);
	auto ptr2 = my_make_unique<string>("text");
	auto ptr3 = my_make_unique<string>();
}

//////////////////////////
// Head - Tail

void print()
{
	cout << "\n";
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail& ... tail)
{
	cout << head << " ";
	print(tail...);
}

TEST_CASE("variadic print")
{
	print(1, 3.14, "text"s);
	// cout << 1; print(3.14, "text");
		// cout << 3.14; print("text");
		   // cout << "text"; print();
				// cout << "\n";


	print("hello", 1);
}