#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <execution>
#include <optional>
#include <functional>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
T my_max(T a, T b)
{
	cout << "Generic impl: ";
	puts(__FUNCSIG__);
	return a < b ? b : a;
}

const char* my_max(const char* a, const char* b)
{
	return strcmp(a, b) < 0 ? b : a;
}

TEST_CASE("function templates")
{    
	auto result1 = my_max(1, 5);

	auto result2 = my_max(3.4, 3.14);

	auto result3 = my_max("abc"s, "def"s);

	REQUIRE(result3 == "def"s);

	auto result4 = my_max<double>(1, 3.14);
	auto result5 = my_max(static_cast<double>(1), 3.14);

	SECTION("specialization")
	{
		auto result = my_max("abc", "def");

		cout << result << endl;
	}
}


template <typename Container>
auto mbegin(Container& c)
{
	return c.begin();
}

template <typename Container>
auto mend(Container& c)
{
	return c.end();
}

template <typename T, size_t N>
auto mbegin(T(&arr)[N])
{
	return arr;
}

template <typename T, size_t N>
auto mend(T(&arr)[N])
{
	return arr + N;
}

TEST_CASE("mbegin + mend")
{
	int tab[] = { 1, 2, 3 };
	for (auto it = mbegin(tab); it != mend(tab); ++it)
		cout << *it << " ";
	cout << "\n";
}

template <typename T>
void deduce1(T arg)
{
	puts(__FUNCSIG__);
}

template <typename T>
void deduce2(T& arg)
{
	puts(__FUNCSIG__);
}

template<typename T>
void deduce3(T&& arg)
{
	puts(__FUNCSIG__);
}

TEST_CASE("auto - type deduction mechanism")
{
	int x = 10;
	const int cx = 20;
	int& rx = x;
	const int& crx = cx;
	int tab[] = { 1, 2, 3 };

	SECTION("case 1")
	{
		deduce1(x);
		auto ax1 = x;
		deduce1(cx);
		auto ax2 = cx;
		deduce1(rx);
		auto ax3 = rx;
		deduce1(crx);
		auto ax4 = crx;
		deduce1(tab);
		auto ax5 = tab;
	}

	SECTION("case 2")
	{
		deduce2(x);
		auto& ax1 = x;
		deduce2(cx);
		auto& ax2 = cx;
		deduce2(rx);
		auto& ax3 = rx;
		deduce2(crx);
		auto& ax4 = crx;
		deduce2(tab);
		auto& ax5 = tab;
	}

	SECTION("case 3")
	{
		deduce3(x);
		auto&& ax1 = x;

		deduce3(10);
		auto&& ax2 = 10;

		deduce3(cx);
		auto&& ax3 = cx;
	}
}

template <typename T1, typename T2>
struct Pair
{
	T1 first;
	T2 second;

	Pair(const T1& f, const T2& s) : first(f), second(s)
	{}

	T1 get_first() const;
	
	T2 get_second() const
	{
		return second;
	}
};

// deduction guide - since C++17
template <typename T1, typename T2> Pair(T1, T2) -> Pair<T1, T2>;
template <typename T> Pair(T, const char*)->Pair<T, string>;

template <typename T1, typename T2>
T1 Pair<T1, T2>::get_first() const
{
	return first;
}

TEST_CASE("class template")
{
	Pair<int, double> p1(1, 3.14);

	REQUIRE(p1.get_first() == 1);
	REQUIRE(p1.get_second() == Approx(3.14));

	Pair<int, string> p2(42, "pair");

	Pair p3(42, "3.13");
	deduce1(p3);

	vector vec = { 1, 2, 3, 4 };
	pair stl_p(1, 3.14);

	int tab[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	vector range(tab + 2, tab + 8);

	for (const auto& item : range)
		cout << item << " ";
	cout << "\n";

	array arr = { 1, 2, 3, 4 };

	auto result = std::reduce(std::execution::par_unseq, begin(tab), end(tab), 0);
}

TEST_CASE("deduction types for class templates (CTAD)")
{
	vector vec = { 1 ,2, 3, 4 };
	pair p{ 1, "text" };
	tuple t{ 1, 3.14, "text"s };
	optional value = 10;
	function f = [](int x) { return to_string(x); };

	unique_ptr<int> ptr(new int(13));
	shared_ptr sptr(std::move(ptr));
	weak_ptr wptr = sptr;
}

template <typename T>
constexpr T dpi = [] { return 2 * 3.14; }();

TEST_CASE("tuple")
{	
	tuple<int, double> t1(1, 3.14);

	REQUIRE(std::get<0>(t1) == 1);

	REQUIRE(dpi<double> == Approx(6.28));
	REQUIRE(dpi<int> == 6);
}