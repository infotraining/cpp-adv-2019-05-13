#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;
using namespace Catch::Matchers;

template <typename Iterator>
void advance_iter_impl(Iterator& it, size_t n, input_iterator_tag)
{
	for (int i = 0; i < n; ++i)
		++it;
}

template <typename Iterator>
void advance_iter_impl(Iterator& it, size_t n, random_access_iterator_tag)
{
	it += n;
}

template <typename Iterator>
void advance_iter(Iterator& it, size_t n)
{
	advance_iter_impl(it, n, typename iterator_traits<Iterator>::iterator_category{});
}

namespace Cpp17
{
	template <typename Iterator>
	void advance_iter(Iterator& it, size_t n)
	{
		if constexpr (
			is_same_v<typename iterator_traits<Iterator>::iterator_category, random_access_iterator_tag>)
		{
			it += n;
		}
		else
		{
			for (int i = 0; i < n; ++i)
				++it;
		}
	}
}

//namespace Cpp20
//{
//	void advance_iter_impl(InputIterator auto& it, size_t n)
//	{
//		for (int i = 0; i < n; ++i)
//			++it;
//	}
//	
//	void advance_iter_impl(RandomAccessIterator auto& it, size_t n)
//	{
//		it += n;
//	}
//}

TEST_CASE("test")
{
	vector vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	auto it = vec.begin();
	advance_iter(it, 3);
	REQUIRE(*it == 4);
}