#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

namespace Step1
{
	template <typename T, typename TResult>
	TResult my_accumulate(const T* first, const T* last, TResult init)
	{
		TResult result = init;

		for (auto it = first; it != last; ++it)
			result += *it;

		return result;
	}
}

namespace Step2
{
	template <typename T>
	struct AccumulationTraits
	{
		using AccumulatorType = T;

		static T init()
		{
			return T{};
		}
	};

	template<>
	struct AccumulationTraits<uint8_t>
	{
		using AccumulatorType = uint64_t;

		static constexpr AccumulatorType init()
		{
			return 0;
		}
	};

	template <>
	struct AccumulationTraits<const char*>
	{
		using AccumulatorType = string;

		static std::string init()
		{
			return "";
		}
	};

	struct Sum
	{
		template <typename T1, typename T2>
		static void accumulate(T1& left, const T2& right)
		{
			left += right;
		}
	};

	struct ScalarTraits
	{
		using AccumulatorType = int;

		static constexpr AccumulatorType init()
		{
			return 1;
		}

	};

	struct Multiply
	{
		template <typename T1, typename T2>
		static void accumulate(T1& left, const T2& right)
		{
			left *= right;
		}
	};

	template <
		typename Iterator, 		
		typename AccTraits = AccumulationTraits<typename iterator_traits<Iterator>::value_type>, 
		typename AccPolicy = Sum>
	typename AccTraits::AccumulatorType my_accumulate(Iterator first, Iterator last)
	{
		using TResult = typename AccTraits::AccumulatorType;
		
		TResult result = AccTraits::init();

		for (auto it = first; it != last; ++it)
			AccPolicy::accumulate(result, *it); // result += *it;

		return result;
	}
}

TEST_CASE("my_accumulate - step1")
{
	using namespace Step1;

	double vec[] = { 1.1, 2, 255 };
	REQUIRE(my_accumulate(vec, vec + 3, 0.0) == Approx(258.1));

	string words[] = { "one", "two", "three" };
	REQUIRE(my_accumulate(words, words + 3, ""s) == "onetwothree"s);
}

TEST_CASE("my_accumulate - step2")
{
	using namespace Step2;

	uint8_t data[] = { 1, 2, 255 };
	REQUIRE(my_accumulate(data, data + 3) == 258);

	vector vec = { 1.1, 2.0, 255.0 };
	REQUIRE(my_accumulate(begin(vec), end(vec)) == Approx(258.1));

	const char* words[] = { "one", "two", "three" };
	REQUIRE(my_accumulate(words, words + 3) == "onetwothree"s);

	vector arr = { 1, 2, 3 };
	REQUIRE(my_accumulate<decltype(begin(arr)), ScalarTraits, Multiply>(begin(arr), end(arr)) == 6);
}

template <typename Iterator>
void foo(Iterator start, Iterator end)
{
	typename iterator_traits<Iterator>::value_type value1 = *start;
	auto a_value1 = *start;

	typename iterator_traits<Iterator>::value_type value2{};
	remove_reference_t<decltype(*start)> a_value2{};
}

TEST_CASE("using traits vs auto")
{
	vector vec = { 1, 2, 3 };
	foo(begin(vec), end(vec));
}