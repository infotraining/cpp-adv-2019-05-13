#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
struct TypeSize
{
	static constexpr size_t value = sizeof(T);
};

template <typename T>
constexpr auto TypeSize_v = TypeSize<T>::value;

template <typename C>
struct ElementType
{
	using type = typename C::value_type;
};

template <typename T, size_t N>
struct ElementType<T[N]>
{
	using type = T;
};

template <typename T>
using ElementType_t = typename ElementType<T>::type;

template <typename Container>
void process(const Container& c)
{
	using T = ElementType_t<Container>;

	T value{};
}

namespace Explain
{
	template <typename T, T v>
	struct IntergralConstant
	{
		static constexpr T value = v;
		using value_type = T;
	};

	template <bool v>
	using BoolConstant = IntergralConstant<bool, v>;

	using TrueType = BoolConstant<true>;
	using FalseType = BoolConstant<false>;



	template <typename T>
	struct IsVoid : FalseType
	{};

	template <>
	struct IsVoid<void> : TrueType
	{};


	template <typename T>
	struct IsPointer : FalseType
	{};

	template <typename T>
	struct IsPointer<T*> : TrueType
	{};
}

template <typename T>
void use(T item)
{
	if constexpr (Explain::IsPointer<T>::value)
	{
		cout << "Delete item... " << *item << endl;
		delete item;
	}
}

TEST_CASE("meta-function")
{
	static_assert(TypeSize_v<int> == 4);

	vector<int> vec;
	static_assert(std::is_same<ElementType<decltype(vec)>::type, int>::value);

	int tab[10];
	static_assert(std::is_same_v<ElementType_t<decltype(tab)>, int>);

	static_assert(Explain::IsVoid<void>::value);

	int* ptr;
	static_assert(Explain::IsPointer<decltype(ptr)>::value);

	ptr = new int(13);

	use(ptr);
	use(10);
}

TEST_CASE("std traits")
{
	auto ll = [](const auto& item) {
		using T = decay_t<decltype(item)>;
		T var{};
	};
}

TEST_CASE("nullptr")
{
	int* ptr = nullptr;
}
