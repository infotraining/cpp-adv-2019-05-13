# cpp-adv-2019-05-13

## Docs

* https://infotraining.bitbucket.io/cpp-adv/


## Libraries

* https://github.com/catchorg/Catch2

## Presentations

* https://www.youtube.com/watch?v=nZNd5FjSquk

## Links

* https://devblogs.microsoft.com/cppblog/two-phase-name-lookup-support-comes-to-msvc/

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-adv-2019-05-13-kp